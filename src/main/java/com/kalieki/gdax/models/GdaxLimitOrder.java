package com.kalieki.gdax.models;

import com.kalieki.gdax.models.trade.GdaxLimitCancelAfter;
import com.kalieki.gdax.models.trade.GdaxLimitOrderTimeInForce;

import static com.kalieki.gdax.models.trade.GdaxOrderTradeType.LIMIT;

public class GdaxLimitOrder extends GdaxTradeOrder {

    private String price;
    private String size;
    private GdaxLimitOrderTimeInForce time_in_force;
    private GdaxLimitCancelAfter cancel_after;
    private boolean post_only = true;

    public GdaxLimitOrder() {
        this.setType(LIMIT);
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public GdaxLimitOrderTimeInForce getTime_in_force() {
        return time_in_force;
    }

    public void setTime_in_force(GdaxLimitOrderTimeInForce time_in_force) {
        this.time_in_force = time_in_force;
    }

    public GdaxLimitCancelAfter getCancel_after() {
        return cancel_after;
    }

    public void setCancel_after(GdaxLimitCancelAfter cancel_after) {
        this.cancel_after = cancel_after;
    }

    public boolean isPost_only() {
        return post_only;
    }

    public void setPost_only(boolean post_only) {
        this.post_only = post_only;
    }
}
