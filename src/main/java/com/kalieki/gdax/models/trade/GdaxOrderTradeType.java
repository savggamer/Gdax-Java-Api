package com.kalieki.gdax.models.trade;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum GdaxOrderTradeType {
    @JsonProperty("limit")
    LIMIT,
    @JsonProperty("market")
    MARKET,
    @JsonProperty("stop")
    STOP,
}
