package com.kalieki.gdax.models.trade;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum GdaxOrderStpFlag {
    @JsonProperty("dc")
    DC,
    @JsonProperty("co")
    CO,
    @JsonProperty("cn")
    CN,
    @JsonProperty("cb")
    CB

}
