package com.kalieki.gdax.models.trade;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum GdaxOrderTradeSide {
    @JsonProperty("buy")
    BUY,
    @JsonProperty("sell")
    SELL,
}
