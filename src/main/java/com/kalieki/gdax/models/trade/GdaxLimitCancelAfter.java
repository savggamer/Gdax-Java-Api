package com.kalieki.gdax.models.trade;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum GdaxLimitCancelAfter {
    @JsonProperty("min")
    MIN,
    @JsonProperty("hour")
    HOUR,
    @JsonProperty("day")
    DAY
}
