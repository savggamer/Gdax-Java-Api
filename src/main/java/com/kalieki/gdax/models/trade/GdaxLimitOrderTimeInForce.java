package com.kalieki.gdax.models.trade;

public enum GdaxLimitOrderTimeInForce {
    GTC,
    GTT,
    IOC,
    FOK;
}
