package com.kalieki.gdax.models;

import com.kalieki.gdax.models.trade.GdaxOrderStpFlag;
import com.kalieki.gdax.models.trade.GdaxOrderTradeSide;
import com.kalieki.gdax.models.trade.GdaxOrderTradeType;

public abstract class GdaxTradeOrder {

    private String client_oid;
    private GdaxOrderTradeType type;
    private GdaxOrderTradeSide side;
    private String product_id;
    private GdaxOrderStpFlag stp;

    public String getClient_oid() {
        return client_oid;
    }

    public void setClient_oid(String client_oid) {
        this.client_oid = client_oid;
    }

    public GdaxOrderTradeType getType() {
        return type;
    }

    public void setType(GdaxOrderTradeType type) {
        this.type = type;
    }

    public GdaxOrderTradeSide getSide() {
        return side;
    }

    public void setSide(GdaxOrderTradeSide side) {
        this.side = side;
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public GdaxOrderStpFlag getStp() {
        return stp;
    }

    public void setStp(GdaxOrderStpFlag stp) {
        this.stp = stp;
    }
}
