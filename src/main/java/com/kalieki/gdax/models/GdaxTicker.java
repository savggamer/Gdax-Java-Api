package com.kalieki.gdax.models;

import java.math.BigInteger;

public class GdaxTicker {

    private BigInteger trade_id;
    private String price;
    private String size;
    private String big;
    private String ask;
    private String volume;
    private String time;

    public BigInteger getTrade_id() {
        return trade_id;
    }

    public void setTrade_id(BigInteger trade_id) {
        this.trade_id = trade_id;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getBig() {
        return big;
    }

    public void setBig(String big) {
        this.big = big;
    }

    public String getAsk() {
        return ask;
    }

    public void setAsk(String ask) {
        this.ask = ask;
    }

    public String getVolume() {
        return volume;
    }

    public void setVolume(String volume) {
        this.volume = volume;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
