package com.kalieki.gdax.impl;

import com.kalieki.gdax.GdaxClient;
import com.kalieki.gdax.config.GdaxAuthorization;
import com.kalieki.gdax.config.GdaxConfiguration;

public class DefaultGdaxClient extends GdaxImpl implements GdaxClient  {



    public DefaultGdaxClient(GdaxConfiguration configuration) {
        super(configuration, new GdaxProducts(configuration), new GdaxAuthorization(configuration));
    }


    @Override
    public GdaxProfile getGdaxProfile() {
        return new GdaxProfile(this.getConfiguration(), this.getProducts(), this.getAuthorization());
    }

    @Override
    public GdaxTrade getGdaxTrade() {
        return new GdaxTrade(this.getConfiguration(), this.getProducts(), this.getAuthorization());
    }


}
