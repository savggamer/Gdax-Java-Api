package com.kalieki.gdax.impl;

import com.kalieki.gdax.Currency;
import com.kalieki.gdax.config.GdaxAuthorization;
import com.kalieki.gdax.config.GdaxConfiguration;
import com.kalieki.gdax.exceptions.GdaxInitializationException;
import com.kalieki.gdax.exceptions.GdaxTradeException;
import com.kalieki.gdax.mapper.GdaxObjectMapper;
import com.kalieki.gdax.models.GdaxOrderResponse;
import com.kalieki.gdax.models.GdaxTradeOrder;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicHeader;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.List;

public class GdaxTrade extends GdaxImpl{

    private static final String PATH = "/orders";

    public GdaxTrade(GdaxConfiguration configuration, GdaxProducts products, GdaxAuthorization authorization) {
        super(configuration, products, authorization);
    }

    public GdaxOrderResponse postTrade(GdaxTradeOrder order){

        try(CloseableHttpClient client = HttpClientBuilder.create().build()){

            HttpPost post = new HttpPost(this.getConfiguration().getBaseUrl() + PATH);
            BasicHeader contentTypeHyder = new BasicHeader("content-type","application/json");
            post.setHeader(contentTypeHyder);


            StringEntity entity = new StringEntity(new GdaxObjectMapper().writeValueAsString(order));
            entity.setContentType(contentTypeHyder);
            post.setEntity(entity);
            getAuthorization().buildAuthHeaders(post);

            try(CloseableHttpResponse response = client.execute(post)){
                if(response.getStatusLine().getStatusCode() == 200){
                    GdaxOrderResponse resp = new GdaxObjectMapper().readValue(response.getEntity().getContent(), GdaxOrderResponse.class);

                    if(resp.getStatus().equalsIgnoreCase("rejected")){
                        throw new GdaxTradeException("Rejected Trade");
                    }

                    return resp;
                }else{
                    System.out.println("ERROR: " + EntityUtils.toString(response.getEntity()));
                }

                throw new GdaxTradeException();
            }
        }catch(Exception e){
            throw new GdaxTradeException(e);
        }
    }

    public boolean cancelOrder(GdaxOrderResponse orderToCancel){
        try(CloseableHttpClient client = HttpClientBuilder.create().build()){

            HttpDelete post = new HttpDelete(this.getConfiguration().getBaseUrl() + PATH + "/" + orderToCancel.getId());
            getAuthorization().buildAuthHeaders(post);

            try(CloseableHttpResponse response = client.execute(post)){
                if(response.getStatusLine().getStatusCode() == 200){
                    return true;
                }else{
                    return false;
                }
            }
        }catch(Exception e){
            throw new GdaxTradeException(e);
        }
    }

    public List<GdaxOrderResponse> getOpenOrders(Currency c1, Currency c2){
        try (CloseableHttpClient httpclient = HttpClients.createDefault()) {
            URIBuilder builder = new URIBuilder(getConfiguration().getBaseUrl() + PATH)
                    .addParameter("product_id", this.getProducts().getProduct(c1, c2).getId());
            HttpGet get = new HttpGet(builder.build());
            this.getAuthorization().buildAuthHeaders(get);
            try (CloseableHttpResponse response = httpclient.execute(get)) {
                HttpEntity entity = response.getEntity();
                String content = EntityUtils.toString(entity);
                GdaxOrderResponse[] arr = new GdaxObjectMapper().readValue(content, GdaxOrderResponse[].class);
                return Arrays.asList(arr);


            }
        } catch (IOException | URISyntaxException e) {
            throw new GdaxInitializationException(e);
        }
    }
}
