package com.kalieki.gdax.impl;

import com.kalieki.gdax.Currency;
import com.kalieki.gdax.config.GdaxConfiguration;
import com.kalieki.gdax.exceptions.GdaxInitializationException;
import com.kalieki.gdax.exceptions.GdaxProductNotFoundException;
import com.kalieki.gdax.exceptions.GdaxRequestException;
import com.kalieki.gdax.mapper.GdaxObjectMapper;
import com.kalieki.gdax.models.Gdax24HourStats;
import com.kalieki.gdax.models.GdaxCandle;
import com.kalieki.gdax.models.GdaxProduct;
import com.kalieki.gdax.models.GdaxTicker;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.joda.time.DateTime;
import org.joda.time.format.ISODateTimeFormat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class GdaxProducts {

    private static final String PRODUCT_PATH = "/products";
    private GdaxConfiguration configuration;

    List<GdaxProduct> productList;

    public GdaxProducts(GdaxConfiguration configuration) {
        this.configuration = configuration;
        try (CloseableHttpClient httpclient = HttpClients.createDefault()) {

            HttpGet get = new HttpGet(configuration.getBaseUrl() + PRODUCT_PATH);
            try (CloseableHttpResponse response = httpclient.execute(get)) {
                HttpEntity entity = response.getEntity();
                GdaxProduct[] arr = new GdaxObjectMapper().readValue(entity.getContent(), GdaxProduct[].class);
                this.productList = Arrays.asList(arr);
            }
        } catch (Exception e) {
            throw new GdaxInitializationException(e);
        }
    }

    public GdaxProduct getProduct(Currency a, Currency b) {
        String ab = a.name() + "-" + b.name();
        String ba = b.name() + "-" + a.name();

        for (GdaxProduct product : productList) {
            if (product.getId().equals(ab) || product.getId().equals(ba)) {
                return product;
            }
        }

        throw new GdaxProductNotFoundException();
    }

    public GdaxTicker getTicker(Currency c1, Currency c2) {
        String id = this.getProduct(c1, c2).getId();
        String path = PRODUCT_PATH + "/" + id + "/ticker";

        try (CloseableHttpClient client = HttpClientBuilder.create().build()) {

            HttpGet get = new HttpGet(configuration.getBaseUrl() + path);
            try (CloseableHttpResponse response = client.execute(get)) {
                HttpEntity entity = response.getEntity();
                return new GdaxObjectMapper().readValue(entity.getContent(), GdaxTicker.class);
            }

        } catch (Exception e) {
            throw new GdaxRequestException(e);
        }
    }

    public List<GdaxCandle> getCandles(Currency c1, Currency c2, DateTime start, DateTime end, int timeslice) {
        String id = this.getProduct(c1, c2).getId();
        String path = PRODUCT_PATH + "/" + id + "/candles?start=" + ISODateTimeFormat.dateTime().print(start.toDateTimeISO()) + "&end=" + ISODateTimeFormat.dateTime().print(end.toDateTimeISO()) + "&granularity=" + timeslice;

        String entityStr = null;
        try (CloseableHttpClient httpclient = HttpClients.createDefault()) {
            HttpGet get = new HttpGet(configuration.getBaseUrl() + path);
            try (CloseableHttpResponse response = httpclient.execute(get)) {
                HttpEntity entity = response.getEntity();
                entityStr = EntityUtils.toString(entity);
                String[][] resp = new GdaxObjectMapper().readValue(entityStr, String[][].class);
                List<GdaxCandle> candles = new ArrayList<>();
                for (int a = 0; a < resp.length; a++) {

                    GdaxCandle candle = new GdaxCandle();
                    for (int b = 0; b < resp[a].length; b++) {
                        Double val = Double.valueOf(resp[a][b]);

                        switch (b) {
                            case 0:
                                candle.setTime(val);
                                break;
                            case 1:
                                candle.setLow(val);
                                break;
                            case 2:
                                candle.setHigh(val);
                                break;
                            case 3:
                                candle.setOpen(val);
                                break;
                            case 4:
                                candle.setClose(val);
                                break;
                            case 5:
                                candle.setVolume(val);
                                break;
                        }
                    }
                    candles.add(candle);
                }

                return candles;
            }
        } catch (Exception e) {
            throw new GdaxRequestException("Response: " + entityStr, e);
        }
    }

    public Gdax24HourStats get24HourStats(Currency c1, Currency c2){
        String id = this.getProduct(c1, c2).getId();
        String path = PRODUCT_PATH + "/" + id + "/stats";

        try (CloseableHttpClient client = HttpClientBuilder.create().build()) {

            HttpGet get = new HttpGet(configuration.getBaseUrl() + path);
            try (CloseableHttpResponse response = client.execute(get)) {
                HttpEntity entity = response.getEntity();
                return new GdaxObjectMapper().readValue(entity.getContent(), Gdax24HourStats.class);
            }

        } catch (Exception e) {
            throw new GdaxRequestException(e);
        }
    }


}

