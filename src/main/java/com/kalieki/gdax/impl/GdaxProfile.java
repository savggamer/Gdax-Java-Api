package com.kalieki.gdax.impl;

import com.kalieki.gdax.Currency;
import com.kalieki.gdax.config.GdaxAuthorization;
import com.kalieki.gdax.config.GdaxConfiguration;
import com.kalieki.gdax.exceptions.GdaxInitializationException;
import com.kalieki.gdax.exceptions.GdaxRequestException;
import com.kalieki.gdax.mapper.GdaxObjectMapper;
import com.kalieki.gdax.models.GdaxAccount;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class GdaxProfile extends GdaxImpl {

    public static final String ACCOUNTS_PATH = "/accounts";

    public Map<Currency, String> idMap = new HashMap<>();


    public GdaxProfile(GdaxConfiguration configuration, GdaxProducts products, GdaxAuthorization authorization) {
        super(configuration, products, authorization);



        try (CloseableHttpClient httpclient = HttpClients.createDefault()) {
            HttpGet get = new HttpGet(configuration.getBaseUrl() + ACCOUNTS_PATH);
            this.getAuthorization().buildAuthHeaders(get);
            try (CloseableHttpResponse response = httpclient.execute(get)) {
                HttpEntity entity = response.getEntity();
                String content = EntityUtils.toString(entity);
                GdaxAccount[] arr = new GdaxObjectMapper().readValue(content, GdaxAccount[].class);
                for (GdaxAccount account : arr) {
                    this.idMap.put(Currency.valueOf(account.getCurrency()), account.getId());
                }

            }
        } catch (IOException e) {
            throw new GdaxInitializationException(e);
        }

    }

    public GdaxAccount getAccountInfo(Currency currency) {

        String id = this.idMap.get(currency);
        try (CloseableHttpClient httpclient = HttpClients.createDefault()) {
            HttpGet get = new HttpGet(getConfiguration().getBaseUrl() + ACCOUNTS_PATH + "/" + id);
            this.getAuthorization().buildAuthHeaders(get);
            try (CloseableHttpResponse response = httpclient.execute(get)) {
                HttpEntity entity = response.getEntity();
                String content = EntityUtils.toString(entity);
                return new GdaxObjectMapper().readValue(content, GdaxAccount.class);
            }
        } catch (Exception e) {
            throw new GdaxRequestException(e);
        }

    }


}

