package com.kalieki.gdax.impl;

import com.kalieki.gdax.config.GdaxAuthorization;
import com.kalieki.gdax.config.GdaxConfiguration;

abstract class GdaxImpl {
    private GdaxConfiguration configuration;
    private GdaxAuthorization authorization;
    private GdaxProducts products;

    public GdaxImpl(GdaxConfiguration configuration, GdaxProducts products, GdaxAuthorization authorization) {
        this.configuration = configuration;
        this.authorization = authorization;
        this.products = products;
    }

    public GdaxConfiguration getConfiguration() {
        return configuration;
    }

    public void setConfiguration(GdaxConfiguration configuration) {
        this.configuration = configuration;
    }

    public GdaxAuthorization getAuthorization() {
        return authorization;
    }

    public void setAuthorization(GdaxAuthorization authorization) {
        this.authorization = authorization;
    }

    public GdaxProducts getProducts() {
        return products;
    }

    public void setProducts(GdaxProducts products) {
        this.products = products;
    }
}
