package com.kalieki.gdax.config;

import com.kalieki.gdax.exceptions.GdaxAuthorizationException;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.digest.HmacUtils;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.message.BasicHeader;
import org.apache.http.util.EntityUtils;
import org.joda.time.DateTime;

import javax.crypto.Mac;
import java.util.ArrayList;
import java.util.List;

public class GdaxAuthorization {

    private static final String CB_ACCESS_KEY = "CB-ACCESS-KEY";
    private static final String CB_ACCESS_SIGN = "CB-ACCESS-SIGN";
    private static final String CB_ACCESS_TIMESTAMP = "CB-ACCESS-TIMESTAMP";
    private static final String CB_ACCESS_PASSPHRASE = "CB-ACCESS-PASSPHRASE";
    private static final String USER_AGENT = "User-Agent";

    private GdaxConfiguration config;

    public GdaxAuthorization(GdaxConfiguration config) {
        this.config = config;
    }

    public void buildAuthHeaders(HttpRequestBase method) {
        try {
            DateTime timestamp = DateTime.now();
            List<BasicHeader> headers = new ArrayList<>();
            headers.add(new BasicHeader(CB_ACCESS_KEY, config.getKey()));
            headers.add(new BasicHeader(CB_ACCESS_PASSPHRASE, config.getPassphrase()));
            headers.add(new BasicHeader(CB_ACCESS_TIMESTAMP, Long.toString(timestamp.getMillis() / 1000)));

            String body = "";
            if(method instanceof HttpPost){
                HttpPost post = (HttpPost) method;
                HttpEntity entity = post.getEntity();
                body = EntityUtils.toString(entity);
            }

            String path = method.getURI().getPath();
            String query = method.getURI().getQuery();
            if(query != null  && query.length() > 0){
                path += "?" + query;
            }
            headers.add(new BasicHeader(CB_ACCESS_SIGN, sign(method.getMethod(), body, path, timestamp)));

            for(BasicHeader header : headers){
                method.addHeader(header);
            }
        } catch (Exception e) {
            throw new GdaxAuthorizationException(e);
        }
    }

    private String sign(String method, String body, String path, DateTime timestamp) {
        String encodedMsg = timestamp.getMillis()/1000 + method + path + body;
        Mac mac = HmacUtils.getHmacSha256(Base64.decodeBase64(config.getSecret().getBytes()));
        return Base64.encodeBase64String(mac.doFinal(encodedMsg.getBytes()));
    }
}
