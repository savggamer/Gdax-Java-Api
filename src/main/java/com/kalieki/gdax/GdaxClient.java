package com.kalieki.gdax;

import com.kalieki.gdax.impl.GdaxProducts;
import com.kalieki.gdax.impl.GdaxProfile;
import com.kalieki.gdax.impl.GdaxTrade;

public interface GdaxClient {
    GdaxProfile getGdaxProfile();
    GdaxTrade getGdaxTrade();
    GdaxProducts getProducts();
}
