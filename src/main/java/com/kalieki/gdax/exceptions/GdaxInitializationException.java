package com.kalieki.gdax.exceptions;

public class GdaxInitializationException extends RuntimeException {

    public GdaxInitializationException() {
    }

    public GdaxInitializationException(String message) {
        super(message);
    }

    public GdaxInitializationException(String message, Throwable cause) {
        super(message, cause);
    }

    public GdaxInitializationException(Throwable cause) {
        super(cause);
    }

    public GdaxInitializationException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
