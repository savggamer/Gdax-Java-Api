package com.kalieki.gdax.exceptions;

public class GdaxRequestException extends RuntimeException {
    public GdaxRequestException() {
    }

    public GdaxRequestException(String message) {
        super(message);
    }

    public GdaxRequestException(String message, Throwable cause) {
        super(message, cause);
    }

    public GdaxRequestException(Throwable cause) {
        super(cause);
    }

    public GdaxRequestException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
