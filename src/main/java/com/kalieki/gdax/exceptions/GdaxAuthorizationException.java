package com.kalieki.gdax.exceptions;

public class GdaxAuthorizationException extends RuntimeException {

    public GdaxAuthorizationException() {
    }

    public GdaxAuthorizationException(String message) {
        super(message);
    }

    public GdaxAuthorizationException(String message, Throwable cause) {
        super(message, cause);
    }

    public GdaxAuthorizationException(Throwable cause) {
        super(cause);
    }

    public GdaxAuthorizationException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
