package com.kalieki.gdax.exceptions;

public class GdaxTradeException extends GdaxRequestException {

    public GdaxTradeException() {
    }

    public GdaxTradeException(String message) {
        super(message);
    }

    public GdaxTradeException(String message, Throwable cause) {
        super(message, cause);
    }

    public GdaxTradeException(Throwable cause) {
        super(cause);
    }

    public GdaxTradeException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
